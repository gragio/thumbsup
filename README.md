# ThumbsUp

### REQUIREMENTS

- [Docker](https://www.docker.com/)

### GET STARTED
After cloning this repo, move to this folder from the terminal and run the followings command:
 
```
$ docker-compose up -d
```
Will return `Creating... done` list message, you can run compose install command:

```
$ docker exec -it thumbsup_fpm composer install
```

Let's add record `thumpsup.localhost` at `127.0.0.1` row inside your system hosts file.

DONE.

[Click here](http://thumpsup.localhost) to see the thumbs up total count.

---

### API

- `POST http://thumpsup.localhost` (no body) will return `201` response to post your thumb up 
- `GET http://thumpsup.localhost/stats` will return `200` response contains the stats about your thumbs up

    ```
    {
        "thumbsUp": {
            "1min": integer,
            "5mins": integer,
            "15mins": integer
        }
    }
    ```