<?php

namespace App\Tests\unit\Controller;

use App\Api\ThumbupCommand;
use App\Controller\PostThumbupController;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PostThumbupControllerTest extends TestCase
{
    /** @var ThumbupCommand */
    private $thumbupCommand;

    /** @var PostThumbupController */
    private $postThumbupController;

    protected function setUp(): void
    {
        parent::setUp();

        $this->thumbupCommand = $this->prophesize(ThumbupCommand::class);
        $this->postThumbupController = new PostThumbupController($this->thumbupCommand->reveal());
    }

    public function testInvoke(): void
    {
        $request = $this->prophesize(Request::class);

        $this->thumbupCommand->add()->shouldBeCalled();

        $response = ($this->postThumbupController)($request->reveal());

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
    }
}