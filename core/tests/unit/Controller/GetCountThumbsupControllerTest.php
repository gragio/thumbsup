<?php

namespace App\Tests\unit\Controller;

use App\Api\ThumbupQuery;
use App\Controller\GetCountThumbsupController;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GetCountThumbsupControllerTest extends TestCase
{
    /** @var ThumbupQuery */
    private $thumbupQuery;

    /** @var GetCountThumbsupController */
    private $getCountThumbsupController;

    protected function setUp(): void
    {
        parent::setUp();

        $this->thumbupQuery = $this->prophesize(ThumbupQuery::class);
        $this->getCountThumbsupController = new GetCountThumbsupController($this->thumbupQuery->reveal());
    }

    public function test__invoke(): void
    {
        $request = $this->prophesize(Request::class);

        $this->thumbupQuery->getCount()->shouldBeCalled();

        $response = ($this->getCountThumbsupController)($request->reveal());

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }
}