<?php


namespace App\Tests\unit\EventSubscriber;

use App\Entity\Thumbup;
use App\EventSubscriber\AddThumbup;
use App\Services\StatsClient;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class AddThumbupTest extends TestCase
{
    /** @var StatsClient */
    private $statsClient;

    /** @var AddThumbup */
    private $addThumbup;

    protected function setUp(): void
    {
        parent::setUp();

        $this->statsClient = $this->prophesize(StatsClient::class);
        $this->addThumbup = new AddThumbup($this->statsClient->reveal());
    }

    public function testPostPersist(): void
    {
        $args = $this->prophesize(LifecycleEventArgs::class);

        $args->getObject()->willReturn(new Thumbup());

        $this->statsClient->request(Request::METHOD_POST, '/')->shouldBeCalled();

        $this->addThumbup->postPersist($args->reveal());
    }
}