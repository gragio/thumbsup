<?php

namespace App\Tests\unit\Api;

use App\Api\ThumbupQuery;
use App\Repository\ThumbupRepository;
use PHPUnit\Framework\TestCase;

class ThumbupQueryTest extends TestCase
{
    /** @var ThumbupRepository */
    private $thumbupRepository;

    /** @var ThumbupQuery */
    private $thumbupQuery;

    protected function setUp(): void
    {
        parent::setUp();

        $this->thumbupRepository = $this->prophesize(ThumbupRepository::class);
        $this->thumbupQuery = new ThumbupQuery($this->thumbupRepository->reveal());
    }

    public function testGetCount(): void
    {
        $this->thumbupRepository->findAll()->willReturn([]);

        $result = $this->thumbupQuery->getCount();
        $this->assertEquals(0, $result);
    }
}