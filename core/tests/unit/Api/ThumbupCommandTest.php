<?php

namespace App\Tests\unit\Api;

use App\Api\ThumbupCommand;
use App\Entity\Thumbup;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class ThumbupCommandTest extends TestCase
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var ThumbupCommand */
    private $thumbupCommand;

    protected function setUp(): void
    {
        parent::setUp();

        $this->em = $this->prophesize(EntityManagerInterface::class);
        $this->thumbupCommand = new ThumbupCommand($this->em->reveal());
    }

    public function testAdd(): void
    {
        $this->em->persist(Argument::type(Thumbup::class))->shouldBeCalled();
        $this->em->flush()->shouldBeCalled();

        $this->thumbupCommand->add();
    }
}