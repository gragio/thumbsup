<?php

namespace App\Api;

use App\Entity\Thumbup;
use Doctrine\ORM\EntityManagerInterface;

class ThumbupCommand
{
    /** @var EntityManagerInterface */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function add(): void
    {
        $this->em->persist(new Thumbup());
        $this->em->flush();
    }
}