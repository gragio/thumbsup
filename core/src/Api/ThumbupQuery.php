<?php

namespace App\Api;

use App\Entity\Thumbup;
use App\Repository\ThumbupRepository;
use Doctrine\ORM\EntityManagerInterface;

class ThumbupQuery
{
    /** @var ThumbupRepository */
    private $thumbupRepository;

    public function __construct(ThumbupRepository $thumbupRepository)
    {
        $this->thumbupRepository = $thumbupRepository;
    }

    public function getCount(): int
    {
        return \count($this->thumbupRepository->findAll());
    }
}