<?php

namespace App\EventSubscriber;

use App\Entity\Thumbup;
use App\Services\StatsClient;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\HttpFoundation\Request;

class AddThumbup implements EventSubscriber
{
    /** @var StatsClient */
    private $statsClient;

    public function __construct(StatsClient $statsClient)
    {
        $this->statsClient = $statsClient;
    }

    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return string[]
     */
    public function getSubscribedEvents()
    {
        return [
            Events::postPersist,
        ];
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        if (!$entity instanceof Thumbup) {
            return;
        }

        $this->statsClient->request(Request::METHOD_POST, '/');
    }
}