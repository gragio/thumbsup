<?php

namespace App\Controller;

use App\Api\ThumbupQuery;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GetCountThumbsupController
{
    /** @var ThumbupQuery */
    private $thumbupQuery;

    public function __construct(ThumbupQuery $thumbupQuery)
    {
        $this->thumbupQuery = $thumbupQuery;
    }

    public function __invoke(Request $request): Response
    {
        $total = $this->thumbupQuery->getCount();

        return Response::create("Total $total thumb(s) up");
    }
}