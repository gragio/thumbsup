<?php

namespace App\Controller;

use App\Services\StatsClient;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class GetStatsController
{
    /** @var StatsClient */
    private $statsClient;

    public function __construct(StatsClient $statsClient)
    {
        $this->statsClient = $statsClient;
    }

    public function __invoke(Request $request)
    {
        $response = $this->statsClient->request(Request::METHOD_GET, '/');

        return JsonResponse::create(
            [
                'thumbsUp' => json_decode($response->getContent(), true),
            ]
        );
    }

}