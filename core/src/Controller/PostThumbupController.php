<?php

namespace App\Controller;

use App\Api\ThumbupCommand;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PostThumbupController
{
    /** @var ThumbupCommand */
    private $thumbupCommand;

    public function __construct(ThumbupCommand $thumbupCommand)
    {
        $this->thumbupCommand = $thumbupCommand;
    }

    public function __invoke(Request $request): JsonResponse
    {
        $this->thumbupCommand->add();

        return JsonResponse::create(null, Response::HTTP_CREATED);
    }

}