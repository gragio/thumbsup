#!/usr/bin/env bash

setfacl -dR -m u:deploy:rwX ./var/cache ./var/logs
setfacl  -R -m u:deploy:rwX ./var/cache ./var/logs

exec php-fpm7.3 -y /etc/php-fpm.conf
