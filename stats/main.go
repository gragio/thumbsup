package main

import (
	"app/api"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func main() {
	e := echo.New()

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.GET("/", api.GetStats)
	e.POST("/", api.PostThumbup)

	e.Logger.Fatal(e.Start(":8000"))
}
