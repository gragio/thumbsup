package cache

import "time"

var m = make(map[int]time.Time, 0)

func Add(t time.Time) {
	m[(len(m) + 1)] = time.Now()
}

func Get() map[int]time.Time {
	return m
}
