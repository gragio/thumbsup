package model

type StatsResponse struct {
	Min1   int `json:"1min"`
	Mins5  int `json:"5mins"`
	Mins15 int `json:"15mins"`
}
