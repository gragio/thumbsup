package api

import (
	"app/cache"
	"net/http"
	"time"

	"github.com/labstack/echo/v4"
)

func PostThumbup(c echo.Context) error {
	cache.Add(time.Now())
	return c.String(http.StatusCreated, `ok`)
}
