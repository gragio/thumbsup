package api

import (
	"app/cache"
	"app/model"
	"net/http"
	"time"

	"github.com/labstack/echo/v4"
)

func GetStats(c echo.Context) error {

	var now1 = time.Now().Add(-1 * time.Minute)
	var now5 = time.Now().Add(-5 * time.Minute)
	var now15 = time.Now().Add(-15 * time.Minute)

	var Min1 = 0
	var Mins5 = 0
	var Mins15 = 0

	for _, element := range cache.Get() {
		if now1.Before(element) {
			Min1++
		}
		if now5.Before(element) {
			Mins5++
		}
		if now15.Before(element) {
			Mins15++
		}
	}

	return c.JSON(
		http.StatusOK,
		model.StatsResponse{
			Min1:   Min1,
			Mins5:  Mins5,
			Mins15: Mins15,
		})
}
